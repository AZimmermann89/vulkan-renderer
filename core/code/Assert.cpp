/*
Alexander Zimmermann 2019
*/
#include "Assert.h"

#include <stdio.h>
#include <Windows.h>

void ReportAssert(const char* expression,
	const char* file,
	const int line,
	const char* message, ...)
{
	if (expression && file && line && message)
	{
		char formatted_text[1024];
		sprintf_s(formatted_text, "Error occured in %s line %d: (%s) %s\n", file, line, expression, message);

		// print to the command line
		printf(formatted_text);

		// print to the debug output
		OutputDebugString(formatted_text);
	}
}

