// Alexander Zimmermann, 2017
#pragma once
#include <code/InputHandler.h>

struct GLFWwindow;
class RenderCore;
class Camera;

const int WIDTH = 800;
const int HEIGHT = 600;

class Application : public InputHandler {
	GLFWwindow* pWindow;
	RenderCore* pRenderer;
	Camera* pCamera;

	bool bFramebufferResized = false;
	static void OnWindowResized(GLFWwindow* window, int width, int height);
	static void OnMouseMove(GLFWwindow* window, double xpos, double ypos);
	static void OnMouseScroll(GLFWwindow* window, double xoffset, double yoffset);

	void InitWindow();
	void CleanUp();
	void MainLoop();

	void ProcessInput(double deltaTime);

	double lastFrameTime;

public:
	Application(RenderCore * pRenderer, Camera * pCamera);
	void Run();	
};