// Alexander Zimmermann, 2019
// https://learnopengl.com/Getting-started/Camera
#pragma once
#include <code/InputHandler.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <GLFW/glfw3.h>


class Camera : public InputListener{
	
	float lastX = 800 / 2.0f;
	float lastY = 600 / 2.0f;
	// Camera Attributes
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;
	// Euler Angles
	float yaw;
	float pitch;
	// Camera options
	float movementSpeed;
	float mouseSensitivity;
	float zoom;
	double lastFrameTime;

	bool isFirstMouseCallback = true;
	bool constrainPitch = true;

public:
	Camera();

	glm::mat4 GetViewMatrix() const;
	
	void UpdateCameraVectors();

	// InputListener
	void ProcessKeyInput(double deltaTime, unsigned int keyPressed) override;
	void ProcessMouseMove(double xPos, double yPos) override;
	void ProcessMouseScroll(double xoffset, double yoffset) override;
	// ~InputListener
};