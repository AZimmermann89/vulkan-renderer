// Alexander Zimmermann, 2019
#pragma once
#include <vector>
#include <code/Assert.h>


enum KeyboardInput {
	PRESSED_W = 1 << 0, 
	PRESSED_A = 1 << 1, 
	PRESSED_S = 1 << 2, 
	PRESSED_D = 1 << 3, 
};

class InputListener {
protected:
	inline bool IsKeyPressed(unsigned int pressedKeys, KeyboardInput queryKey) const {
		return pressedKeys & queryKey;
	};
public:
	virtual void ProcessKeyInput(double deltaTime, unsigned int keyPressed) = 0;
	virtual void ProcessMouseMove(double xpos, double ypos) = 0;
	virtual void ProcessMouseScroll(double xoffset, double yoffset) = 0;
};


class InputHandler 
{
	std::vector<std::reference_wrapper<InputListener>> listeners;

protected:

	inline void ActivateKeyPress(unsigned int & keyPressed, KeyboardInput ki)
	{
		if (!(keyPressed & ki)) keyPressed |= ki;
	}

	inline void DeactivateKeyPress(unsigned int& keyPressed, KeyboardInput ki)
	{
		if ((keyPressed & ki)) keyPressed &= ~ki;
	}

	void UpdateListenersKeypress(double deltaTime, unsigned int keyPressed)
	{
		for (InputListener& il : listeners) {
			il.ProcessKeyInput(deltaTime, keyPressed);
		}
	}

	void UpdateListenersMouseMove(double xpos, double ypos)
	{
		for (InputListener& il : listeners) {
			il.ProcessMouseMove(xpos, ypos);
		}
	}

	void UpdateListenersMouseScroll(double xoffset, double yoffset)
	{
		for (InputListener& il : listeners) {
			il.ProcessMouseScroll(xoffset, yoffset);
		}
	}

public:
	void RegisterListener(InputListener & listener) {
		listeners.push_back(listener);
	}
	void UnregisterListener(InputListener & listener) {

		//auto it = std::find_if(std::begin(observers_), std::end(observers_), [&ob](IObserver& element) -> bool
		//	{
		//		return &element == &ob;
		//	});

		//observers_.erase(it);
		GM_ASSERT_MSG(false, "TODO (from 11 july 2019): For unregistering I should either override the == operator in the Listener to compare listeners and find the 'current' one, or not use a Vector. Not sure which way to go yet.");
	}
};
