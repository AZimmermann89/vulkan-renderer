#pragma once

// Can be altered to update the version number.
// Engine Version and Application Version don't need to be the same.
#ifndef VERSION_AZ
	#define VERSION_AZ VK_MAKE_VERSION(1,0,0);
#endif //VERSION_AZ

#ifndef VERSION_ENG_AZ
	#define VERSION_ENG_AZ VERSION_AZ
#endif //VERSION_ENG_AZ

#ifndef VERSION_APP_AZ
	#define VERSION_APP_AZ VERSION_AZ
#endif //VERSION_APP_AZ