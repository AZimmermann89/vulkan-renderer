// Alexander Zimmermann, 2019
#include "Camera.h"
#include <code/Assert.h>

Camera::Camera()
	: position(glm::vec3(0.0f, -5.0f, 1.0f))
	, front(glm::vec3(0.0f, 1.0f, 0.0f))
	, up(glm::vec3(0.0f, 0.0f, 1.0f))
	, worldUp(glm::vec3(0.0f, 0.0f, 1.0f))
	, isFirstMouseCallback(true)
	, yaw(-90.f)
	, pitch(0.0f)
	, movementSpeed(10.f)
	, mouseSensitivity(1.f)
	, zoom(45.0f)
	, lastFrameTime(-1.0f)
{	
}

glm::mat4 Camera::GetViewMatrix() const
{
	return glm::lookAt(position, position + front, up);
}

void Camera::UpdateCameraVectors()
{
	// LOG
	char formatted_text[1024];
	sprintf_s(formatted_text, "sin(glm::radians(pitch)) %f\n", sin(glm::radians(pitch)));
	printf(formatted_text);
	//fflush(stdout);
	// LOG

	// Calculate the new Front vector
	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	//front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	//front.y = sin(glm::radians(pitch));
	//front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(front);
	// Also re-calculate the Right and Up vector
	right = glm::normalize(glm::cross(front, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	up = glm::normalize(glm::cross(right, front));
}

void Camera::ProcessKeyInput(double deltaTime, unsigned int keyPressed)
{
	float velocity = movementSpeed * deltaTime;

	if (IsKeyPressed(keyPressed, PRESSED_W))
	{
		position += velocity * front;
	}
	if (IsKeyPressed(keyPressed, PRESSED_S))
	{
		position -= velocity * front;
	}
	if (IsKeyPressed(keyPressed, PRESSED_A))
	{
		position -= right * velocity;
	}
	if (IsKeyPressed(keyPressed, PRESSED_D))
	{
		position += right * velocity;
	}
	
}

void Camera::ProcessMouseMove(double xPos, double yPos)
{

	if (isFirstMouseCallback)
	{
		lastX = xPos;
		lastY = yPos;
		isFirstMouseCallback = false;
	}

	float xOffset = xPos - lastX;
	float yOffset = yPos - lastY;

	lastX = xPos;
	lastY = yPos;

	xOffset *= mouseSensitivity;
	yOffset *= mouseSensitivity;


	yaw += xOffset;
	pitch += yOffset;
	
	if (constrainPitch)
	{
		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;
	}
	
	UpdateCameraVectors();
}

void Camera::ProcessMouseScroll(double xoffset, double yOffset)
{
	if (zoom >= 1.0f && zoom <= 45.0f)
		zoom -= yOffset;
	if (zoom <= 1.0f)
		zoom = 1.0f;
	if (zoom >= 45.0f)
		zoom = 45.0f;
}
