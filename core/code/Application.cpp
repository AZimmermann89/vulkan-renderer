#include "Application.h"
#include <code/Assert.h>
#include <code/Camera.h>
#include <code/RenderCore.h>

#include <GLFW/glfw3.h>


Application::Application(RenderCore* pRenderer, Camera* pCamera)
	: pRenderer(pRenderer)
	, pCamera(pCamera)
	, lastFrameTime(0.0f)
{
}

void Application::Run()
{
	InitWindow();
	pRenderer->Init(pWindow);
	MainLoop();
	CleanUp();
}

void Application::InitWindow()
{
	GM_ASSERT_MSG(glfwInit() == GLFW_TRUE, "Failed to initialize glfw.");
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	GM_ASSERT_MSG(pWindow = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan Core", nullptr, nullptr), "Failed to create the glfw window.");

	glfwSetWindowUserPointer(pWindow, this);
	glfwSetWindowSizeCallback(pWindow, Application::OnWindowResized);
	glfwSetCursorPosCallback(pWindow, Application::OnMouseMove);
	glfwSetScrollCallback(pWindow, Application::OnMouseScroll);
	
	glfwSetInputMode(pWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Application::OnWindowResized(GLFWwindow* window, int width, int height)
{
	if (width == 0 || height == 0) return;

	Application* app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
	app->bFramebufferResized = true;
}

void Application::OnMouseMove(GLFWwindow* window, double xpos, double ypos)
{
	Application* app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
	app->UpdateListenersMouseMove(xpos, ypos);
}

void Application::OnMouseScroll(GLFWwindow* window, double xoffset, double yoffset)
{
	Application* app = reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
	app->UpdateListenersMouseScroll(xoffset, yoffset);
}

void Application::CleanUp()
{
	pRenderer->CleanUp();

	glfwDestroyWindow(pWindow);
	glfwTerminate();
}

void Application::MainLoop()
{
	while (!glfwWindowShouldClose(pWindow))
	{
		double currentFrameTime = glfwGetTime();
		double deltaTime = currentFrameTime - lastFrameTime;
		lastFrameTime = currentFrameTime;

		ProcessInput(deltaTime);
		glfwPollEvents();
		pRenderer->DrawFrame();
	}

	pRenderer->WaitIdle();
}

void Application::ProcessInput(double deltaTime)
{
	unsigned int keyPressed = 0;

	if (glfwGetKey(pWindow, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(pWindow, true);

	if (glfwGetKey(pWindow, GLFW_KEY_W) == GLFW_PRESS) {
		ActivateKeyPress(keyPressed, KeyboardInput::PRESSED_W);
	}
	if (glfwGetKey(pWindow, GLFW_KEY_S) == GLFW_PRESS) {
		ActivateKeyPress(keyPressed, KeyboardInput::PRESSED_S);		
	}
	if (glfwGetKey(pWindow, GLFW_KEY_A) == GLFW_PRESS) {
		ActivateKeyPress(keyPressed, KeyboardInput::PRESSED_A);		
	}
	if (glfwGetKey(pWindow, GLFW_KEY_D) == GLFW_PRESS) {
		ActivateKeyPress(keyPressed, KeyboardInput::PRESSED_D);		
	}

	UpdateListenersKeypress(deltaTime, keyPressed);
}